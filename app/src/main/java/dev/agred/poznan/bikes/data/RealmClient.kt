package dev.agred.poznan.bikes.data

import android.content.Context
import dev.agred.poznan.bikes.data.entity.StationEntity
import dev.agred.poznan.bikes.data.entity.StationsEntity
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.RealmList
import java.util.*

object RealmClient {


    private lateinit var realm: Realm

    val stations: RealmList<StationEntity>
        get() {
            return realm.where(StationsEntity::class.java).findFirst()!!.stations
        }

    val lastUpdateDate: Date
        get() {
            val ret = realm.where(StationsEntity::class.java).findFirst() ?: return Calendar.getInstance().time
            return ret.lastUpdate
        }

    fun initialize(context: Context) {
        Realm.init(context)
        val config = RealmConfiguration.Builder()
            .name("stations.db")
            .schemaVersion(1)
            .deleteRealmIfMigrationNeeded()
            .build()
        realm = Realm.getInstance(config)
    }

    fun saveStations(stations: List<StationEntity>) {
        realm.executeTransaction {
            val findAll = realm.where(StationsEntity::class.java).findAll()
            if (findAll != null)
                realm.delete(StationsEntity::class.java)

            val stationsEntities = realm.createObject(StationsEntity::class.java)
            stationsEntities.stations = RealmList()
            stationsEntities.stations.addAll(stations)
            stationsEntities.lastUpdate = Calendar.getInstance().time
            realm.copyToRealm(stationsEntities)
        }
    }

    fun getStation(id: String): StationEntity? {
        return realm.where(StationEntity::class.java).equalTo("id", id).findFirst()
    }

}