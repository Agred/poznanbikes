package dev.agred.poznan.bikes.ui.fragment.stationlist

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import dev.agred.poznan.bikes.R

class StationListHolder(view: View) : RecyclerView.ViewHolder(view) {

    init {
        ButterKnife.bind(this, view)
    }

    @BindView(R.id.layout)
    lateinit var layout: View
    @BindView(R.id.title)
    lateinit var title: TextView
    @BindView(R.id.address)
    lateinit var address: TextView
    @BindView(R.id.bikes_availability)
    lateinit var bikesAvailability: TextView
    @BindView(R.id.places_availability)
    lateinit var placesAvailability: TextView
}