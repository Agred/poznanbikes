package dev.agred.poznan.bikes.data.entity

import io.realm.RealmList
import io.realm.RealmModel
import io.realm.annotations.RealmClass
import java.util.*

@RealmClass
open class StationsEntity : RealmModel {
    lateinit var stations: RealmList<StationEntity>
    lateinit var lastUpdate: Date
}