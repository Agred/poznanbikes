package dev.agred.poznan.bikes.ui.fragment.stationlist

import android.content.Context
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import dev.agred.poznan.bikes.R
import dev.agred.poznan.bikes.ui.activity.MainActivity
import dev.agred.poznan.bikes.ui.fragment.BaseFragment

class StationListFragment : BaseFragment(), StationListView {
    companion object {
        fun newInstance() = StationListFragment()
    }

    @BindView(R.id.bikes_list)
    lateinit var listView: RecyclerView

    @BindView(R.id.last_update_text)
    lateinit var lastUpdated: TextView

    @BindView(R.id.refresh_layout)
    lateinit var refreshLayout: SwipeRefreshLayout

    private val presenter: StationListPresenter = StationListPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.saveState(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val layout = inflater.inflate(R.layout.fragment_station_list, container, false)
        unbinder = ButterKnife.bind(this, layout)
        presenter.restoreState(savedInstanceState)
        context?.let {
            presenter.initRecyclerView(it)
        }
        presenter.setLastUpdate()
        refreshLayout.setOnRefreshListener(presenter)
        return layout
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        presenter.saveState(outState)
    }

    override fun setLastUpdatedView(lastUpdate: String) {
        val lastUpdateText = getText(R.string.last_update)
        val lastUpdateValue = "$lastUpdateText $lastUpdate"
        lastUpdated.text = lastUpdateValue
    }

    override fun getRecyclerView(): RecyclerView {
        return listView
    }

    override fun getViewContext(): Context? {
        return context
    }

    override fun setDetailsFragment(stationId: String) {
        (activity as MainActivity?)?.setDetailsFragment(stationId)
    }

    override fun getRefreshView(): SwipeRefreshLayout {
        return refreshLayout
    }
}
