package dev.agred.poznan.bikes.utils

import dev.agred.poznan.bikes.connection.dto.StationListDto
import dev.agred.poznan.bikes.data.entity.StationEntity

class DtoConverter {
    companion object {
        fun stationDetailsDtoToStationEntity(dto: StationListDto): List<StationEntity> {
            val result = ArrayList<StationEntity>()

            for ((details, location, id) in dto.stationList) {
                var latitude: Double? = null
                var longitude: Double? = null
                location.coordinates.let {
                    if (it.size < 2)
                        return@let
                    latitude = it[1]
                    longitude = it[0]
                }

                if (longitude == null || latitude == null)
                    continue

                val entity = StationEntity(
                    id,
                    details.freeRacks,
                    details.bikesCount,
                    details.placeName,
                    details.bikeRacks,
                    latitude!!,
                    longitude!!
                )
                result.add(entity)
            }

            return result
        }
    }
}