package dev.agred.poznan.bikes.data

interface TransactionListener {
    fun onSuccess()
    fun onFailure(t: Throwable)
}