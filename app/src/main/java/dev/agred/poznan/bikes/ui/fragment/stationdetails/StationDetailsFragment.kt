package dev.agred.poznan.bikes.ui.fragment.stationdetails


import android.location.Location
import android.location.LocationListener
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import dev.agred.poznan.bikes.R
import dev.agred.poznan.bikes.data.RealmClient
import dev.agred.poznan.bikes.ui.fragment.BaseFragment
import dev.agred.poznan.bikes.utils.LocationProvider
import org.osmdroid.views.MapView

class StationDetailsFragment : BaseFragment(), LocationListener {
    companion object {
        private const val ARG_STATION_ID = "station_id"

        fun newInstance(stationId: String): StationDetailsFragment {
            return StationDetailsFragment().also { fragment ->
                fragment.arguments = Bundle().also { bundle ->
                    bundle.putString(ARG_STATION_ID, stationId)
                }
            }
        }
    }

    @BindView(R.id.map_view)
    lateinit var map: MapView
    @BindView(R.id.title)
    lateinit var title: TextView
    @BindView(R.id.bikes_availability)
    lateinit var bikeAvailability: TextView
    @BindView(R.id.places_availability)
    lateinit var placeAvailability: TextView

    private lateinit var presenter: StationDetailsPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val id = fetchStationArgument()
        val station = RealmClient.getStation(id)
        if (station == null) {
            Toast.makeText(context, R.string.station_entity_missing, Toast.LENGTH_LONG).show()
            return
        }

        presenter = StationDetailsPresenter(station, context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val layout = inflater.inflate(R.layout.fragment_station_details, container, false)
        unbinder = ButterKnife.bind(this, layout)
        presenter.initializeViews(title, bikeAvailability, placeAvailability, map)

        LocationProvider.getLocation(context, this)
        return layout
    }

    override fun onResume() {
        super.onResume()
        map.onResume()
    }

    override fun onPause() {
        super.onPause()
        map.onPause()
    }

    override fun onLocationChanged(location: Location?) {
        if (location == null || this.isRemoving || this.isDetached || !this.isVisible)
            return
        presenter.addPoint(location, map)
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
    }

    override fun onProviderEnabled(provider: String?) {
    }

    override fun onProviderDisabled(provider: String?) {
    }

    private fun fetchStationArgument(): String {
        return arguments?.getString(ARG_STATION_ID) ?: throw Throwable("Fragment was created without station id")
    }

}
