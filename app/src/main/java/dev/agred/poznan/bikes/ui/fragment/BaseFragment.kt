package dev.agred.poznan.bikes.ui.fragment

import android.support.v4.app.Fragment
import butterknife.Unbinder

abstract class BaseFragment : Fragment() {
    protected lateinit var unbinder: Unbinder

    override fun onDestroyView() {
        super.onDestroyView()
        unbinder.unbind()
    }

}