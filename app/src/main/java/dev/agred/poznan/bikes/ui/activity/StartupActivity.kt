package dev.agred.poznan.bikes.ui.activity

import android.content.Intent
import android.os.Bundle
import android.widget.ProgressBar
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import dev.agred.poznan.bikes.R
import dev.agred.poznan.bikes.connection.RestServiceClient
import dev.agred.poznan.bikes.connection.StationListObserver
import dev.agred.poznan.bikes.data.RealmClient
import dev.agred.poznan.bikes.data.entity.StationEntity

class StartupActivity : BaseActivity(), StationListObserver {
    @BindView(R.id.progress)
    lateinit var progress: ProgressBar

    private val rest = RestServiceClient()
    private var errorCounter = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_startup)
        unbinder = ButterKnife.bind(this)
        RealmClient.initialize(applicationContext)
    }

    override fun onStart() {
        super.onStart()
        progress.progress = 0
        downloadStationList()
    }

    private fun downloadStationList() {
        rest.getStationList(this)
        progress.progress = 25
    }

    override fun onSuccess(dto: List<StationEntity>) {
        progress.progress = 75
        RealmClient.saveStations(dto)
        finishDownloading()
    }

    private fun finishDownloading() {
        progress.progress = 100
        startActivity(Intent(this, MainActivity::class.java))
    }

    override fun onFailure(e: Throwable) {
        progress.progress = 0
        Toast.makeText(this, e.localizedMessage, Toast.LENGTH_LONG).show()

        errorCounter++
        if (errorCounter < 3)
            downloadStationList()
        else
            Toast.makeText(applicationContext, R.string.toast_downloading_data_error, Toast.LENGTH_LONG).show()
    }
}
