package dev.agred.poznan.bikes.ui.fragment

import android.support.v4.app.FragmentManager
import dev.agred.poznan.bikes.R
import dev.agred.poznan.bikes.ui.fragment.stationdetails.StationDetailsFragment
import dev.agred.poznan.bikes.ui.fragment.stationlist.StationListFragment

class FragmentHelper(private val fragmentManager: FragmentManager, private val container: Int) {
    fun getBackStackEntryCount(): Int {
        return fragmentManager.backStackEntryCount
    }

    fun popBackStack() {
        fragmentManager.popBackStack()
    }

    fun clearBackStack() {
        for (i in 0 until fragmentManager.backStackEntryCount)
            fragmentManager.popBackStack()
    }

    fun setStationListFragment() {
        val fragment = StationListFragment.newInstance()
        fragmentManager.beginTransaction().replace(container, fragment).commit()
    }

    fun setDetailsFragment(stationId: String) {
        fragmentManager.beginTransaction()
            .addToBackStack("")
            .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
            .replace(container, StationDetailsFragment.newInstance(stationId))
            .commit()
    }
}
