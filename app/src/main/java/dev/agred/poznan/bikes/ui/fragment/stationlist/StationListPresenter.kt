package dev.agred.poznan.bikes.ui.fragment.stationlist

import android.content.Context
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Toast
import dev.agred.poznan.bikes.R
import dev.agred.poznan.bikes.connection.RestServiceClient
import dev.agred.poznan.bikes.connection.StationListObserver
import dev.agred.poznan.bikes.data.RealmClient
import dev.agred.poznan.bikes.data.entity.StationEntity

class StationListPresenter(private val view: StationListView) : SwipeRefreshLayout.OnRefreshListener,
    StationListObserver {
    companion object {
        const val STATE_SCROLL_POSITION = "scroll_state"
    }

    private lateinit var refreshLayout: SwipeRefreshLayout
    private lateinit var recyclerView: RecyclerView

    private var listScrollPosition = 0
    private var listAdapter: StationListAdapter = StationListAdapter(view, RealmClient.stations)

    fun saveState(bundle: Bundle?) {
        bundle?.putInt(STATE_SCROLL_POSITION, listScrollPosition)
    }

    fun restoreState(bundle: Bundle?) {
        listScrollPosition = bundle?.getInt(STATE_SCROLL_POSITION) ?: listScrollPosition
    }

    fun setLastUpdate() {
        view.setLastUpdatedView(RealmClient.lastUpdateDate.toString())
    }

    fun initRecyclerView(context: Context) {
        recyclerView = view.getRecyclerView()
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = listAdapter
        (recyclerView.layoutManager as LinearLayoutManager).scrollToPosition(listScrollPosition)
        recyclerView.scrollTo(0, listScrollPosition)
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                listScrollPosition = (recyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
            }
        })
    }

    override fun onRefresh() {
        refreshLayout = view.getRefreshView()
        refreshLayout.isRefreshing = true
        RestServiceClient().getStationList(this)
    }

    override fun onSuccess(dto: List<StationEntity>) {
        RealmClient.saveStations(dto)
        listAdapter.updateList(dto)
        setLastUpdate()
        refreshLayout.isRefreshing = false
    }

    override fun onFailure(e: Throwable) {
        Toast.makeText(view.getViewContext(), R.string.toast_downloading_data_error, Toast.LENGTH_LONG).show()
        refreshLayout.isRefreshing = false
    }
}