package dev.agred.poznan.bikes.utils

import android.content.Context
import android.location.LocationListener
import android.location.LocationManager

object LocationProvider {

    const val LOCATION_INTERVAL = 0L
    const val LOCATION_DISTANCE = 0.0f

    fun getLocation(context: Context?, listener: LocationListener) {
        if (context == null)
            return
        val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        try {
            locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                LOCATION_INTERVAL,
                LOCATION_DISTANCE,
                listener
            )
            locationManager.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER,
                LOCATION_INTERVAL,
                LOCATION_DISTANCE,
                listener
            )
        } catch (e: SecurityException) {
        } catch (e: IllegalArgumentException) {

        }
    }
}