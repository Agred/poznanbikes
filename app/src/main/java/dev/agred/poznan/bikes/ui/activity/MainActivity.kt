package dev.agred.poznan.bikes.ui.activity

import android.os.Bundle
import butterknife.ButterKnife
import dev.agred.poznan.bikes.R
import dev.agred.poznan.bikes.ui.fragment.FragmentHelper
import dev.agred.poznan.bikes.utils.PermissionHelper

class MainActivity : BaseActivity() {

    companion object {
        const val LOCATION_PERMISSION_REQUEST_CODE = 100
        const val MEMORY_PERMISSION_REQUEST_CODE = 200
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        unbinder = ButterKnife.bind(this)
        fragmentHelper = FragmentHelper(supportFragmentManager, R.id.fragment_container)

        checkPermission()
        initializeFragment()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        PermissionHelper.handleResult(requestCode, permissions, grantResults, applicationContext)
    }

    fun setDetailsFragment(stationId: String) {
        fragmentHelper.setDetailsFragment(stationId)
    }

    private fun checkPermission() {
        if (!PermissionHelper.haveLocationPermission(this)) {
            PermissionHelper.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE)
        }
        if (!PermissionHelper.haveExternalStoragePermission(this)) {
            PermissionHelper.requestPermission(this, MEMORY_PERMISSION_REQUEST_CODE)
        }
    }

    private fun initializeFragment() {
        if (fragmentHelper.getBackStackEntryCount() == 0)
            fragmentHelper.setStationListFragment()
    }
}
