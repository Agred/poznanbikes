package dev.agred.poznan.bikes.connection.dto

import com.google.gson.annotations.SerializedName

data class StationDataDto(
    @SerializedName("properties")
    val details: StationDetailsDto,
    @SerializedName("geometry")
    val location: LocationDto,
    val id: String
)