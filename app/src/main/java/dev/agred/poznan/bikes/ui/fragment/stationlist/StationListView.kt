package dev.agred.poznan.bikes.ui.fragment.stationlist

import android.content.Context
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.RecyclerView

interface StationListView {
    fun setLastUpdatedView(lastUpdate: String)
    fun getRecyclerView(): RecyclerView
    fun getRefreshView(): SwipeRefreshLayout
    fun getViewContext(): Context?
    fun setDetailsFragment(stationId: String)
}