package dev.agred.poznan.bikes.connection.dto

import com.google.gson.annotations.SerializedName

data class StationDetailsDto(
    @SerializedName("free_racks")
    val freeRacks: String,
    @SerializedName("bikes")
    val bikesCount: String,
    @SerializedName("label")
    val placeName: String,
    @SerializedName("bike_racks")
    val bikeRacks: String
)