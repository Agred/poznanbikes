package dev.agred.poznan.bikes.connection

import dev.agred.poznan.bikes.connection.dto.StationListDto
import dev.agred.poznan.bikes.utils.DtoConverter
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class RestServiceClient {

    private val client: RestService = Retrofit.Builder()
        .baseUrl(RestService.HOST)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
        .create(RestService::class.java)

    fun getStationList(observer: StationListObserver) {
        client.getBikesData()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(generateRestObserver(observer))
    }

    private fun generateRestObserver(observer: StationListObserver): Observer<StationListDto> {
        return object : Observer<StationListDto> {
            override fun onComplete() {}

            override fun onSubscribe(d: Disposable) {}

            override fun onNext(dto: StationListDto) {
                val stations = DtoConverter.stationDetailsDtoToStationEntity(dto)
                observer.onSuccess(stations)
            }

            override fun onError(e: Throwable) {
                observer.onFailure(e)
            }
        }
    }
}