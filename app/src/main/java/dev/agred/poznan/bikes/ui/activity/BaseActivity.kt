package dev.agred.poznan.bikes.ui.activity

import android.support.v7.app.AppCompatActivity
import butterknife.Unbinder
import dev.agred.poznan.bikes.ui.fragment.FragmentHelper

abstract class BaseActivity : AppCompatActivity() {

    protected lateinit var unbinder: Unbinder
    protected lateinit var fragmentHelper: FragmentHelper

    override fun onDestroy() {
        super.onDestroy()
        unbinder.unbind()
    }

    override fun onBackPressed() {
        if (fragmentHelper.getBackStackEntryCount() > 0) {
            fragmentHelper.popBackStack()
            return
        }
        super.onBackPressed()
    }
}