package dev.agred.poznan.bikes.ui.fragment.stationdetails

import android.content.Context
import android.location.Location
import android.preference.PreferenceManager
import android.widget.TextView
import dev.agred.poznan.bikes.BuildConfig
import dev.agred.poznan.bikes.data.entity.StationEntity
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.CustomZoomButtonsController
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.Marker

class StationDetailsPresenter(
    val entity: StationEntity,
    val context: Context?
) {

    private var currentLocation: Marker? = null

    fun initializeViews(
        title: TextView,
        bikeAvailability: TextView,
        placeAvailability: TextView,
        map: MapView
    ) {
        title.text = entity.placeName
        bikeAvailability.text = entity.bikesCount
        placeAvailability.text = entity.freeRacks
        initializeMap(map)
    }

    fun addPoint(location: Location, map: MapView) {
        if (currentLocation == null) {
            currentLocation = Marker(map)
            map.overlays.add(currentLocation)
            currentLocation?.position = GeoPoint(location)
        } else
            currentLocation?.position = GeoPoint(location)
        map.invalidate()
    }

    private fun initializeMap(map: MapView) {
        val configuration = Configuration.getInstance()
        configuration.load(context, PreferenceManager.getDefaultSharedPreferences(context))
        configuration.userAgentValue = BuildConfig.APPLICATION_ID

        map.setTileSource(TileSourceFactory.MAPNIK)
        map.zoomController.setVisibility(CustomZoomButtonsController.Visibility.SHOW_AND_FADEOUT)
        map.setMultiTouchControls(true)
        map.controller.setZoom(18.5)

        val point = GeoPoint(entity.latitude, entity.longitude)
        addPoint(map, point)
        map.controller.setCenter(point)
        map.invalidate()
    }

    private fun addPoint(map: MapView, point: GeoPoint) {
        val mapPoint = Marker(map)
        mapPoint.position = point
        mapPoint.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM)
        mapPoint.title = entity.placeName
        map.overlays.add(mapPoint)
    }
}