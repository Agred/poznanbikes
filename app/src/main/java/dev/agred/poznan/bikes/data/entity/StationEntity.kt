package dev.agred.poznan.bikes.data.entity

import io.realm.RealmModel
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

@RealmClass
open class StationEntity() : RealmModel {

    @PrimaryKey
    lateinit var id: String
    lateinit var freeRacks: String
    lateinit var bikesCount: String
    lateinit var placeName: String
    lateinit var bikeRacks: String
    var latitude: Double = 0.0
    var longitude: Double = 0.0

    constructor(
        id: String,
        freeRacks: String,
        bikesCount: String,
        placeName: String,
        bikeRacks: String,
        latitude: Double,
        longitude: Double
    ) : this() {
        this.id = id
        this.freeRacks = freeRacks
        this.bikesCount = bikesCount
        this.placeName = placeName
        this.bikeRacks = bikeRacks
        this.latitude = latitude
        this.longitude = longitude
    }


}