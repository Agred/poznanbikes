package dev.agred.poznan.bikes.connection

import dev.agred.poznan.bikes.data.entity.StationEntity

interface StationListObserver {
    fun onSuccess(dto: List<StationEntity>)
    fun onFailure(e: Throwable)
}