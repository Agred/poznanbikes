package dev.agred.poznan.bikes.connection.dto

import com.google.gson.annotations.SerializedName

data class StationListDto(
    @SerializedName("features")
    var stationList: List<StationDataDto>
)