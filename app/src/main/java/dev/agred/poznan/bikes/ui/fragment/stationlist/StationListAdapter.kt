package dev.agred.poznan.bikes.ui.fragment.stationlist

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import dev.agred.poznan.bikes.R
import dev.agred.poznan.bikes.data.entity.StationEntity

class StationListAdapter(
    private val view: StationListView,
    private var stations: List<StationEntity>
) : RecyclerView.Adapter<StationListHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StationListHolder {
        return StationListHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_station, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return stations.size
    }

    override fun onBindViewHolder(holder: StationListHolder, position: Int) {
        val item = getItem(position)
        holder.layout.setOnClickListener { view.setDetailsFragment(item!!.id) }
        holder.title.text = item?.placeName ?: ""
        holder.bikesAvailability.text = item?.bikesCount ?: "0"
        holder.placesAvailability.text = item?.bikeRacks ?: "0"
    }

    fun updateList(list: List<StationEntity>) {
        stations = list
        this.notifyDataSetChanged()
    }

    private fun getItem(position: Int): StationEntity? {
        if (position < 0 || position > stations.size) return null
        return stations[position]
    }
}